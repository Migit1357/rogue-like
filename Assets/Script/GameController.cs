﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private GameObject startImage;
    private Text levelText;
    private Text startText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int secondsUntilDayStarts = 10;
    private int currentLevel = 1;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

    void Start()
    {


        Application.LoadLevel(Application.loadedLevel);
        currentLevel = 0;
        InitaializeGame();

    }



    private void InitaializeGame()
    {
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        settingUpGame = true;
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);

    }

    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;

    }


    private void OnLevelWasLoaded(int levelLoaded)
    {

        currentLevel++;
        InitaializeGame();
    }

    void Update()
    {
        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);


        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;

    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    private bool CheckingHighscore()
    {
        if (currentLevel > PlayerPrefs.GetInt("highscore", 0))
        {
            return true;
        }
        else return false;
    }

    private int GetHighscore ()
    {
        if( CheckingHighscore() )
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0);
    }




    public void GameOver()
    {
        bool brokeHighscore = CheckingHighscore();
        int highscore = GetHighscore();
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        if (brokeHighscore)
        {
            levelText.text = "You starved after " + currentLevel + " days... \n\n" + "New Highscore: " + highscore;
            levelImage.SetActive(true);
        }
        else
        {
            levelText.text = "You starved after " + currentLevel + " days... \n\n" + "Previous Highscore: " + highscore;
            levelImage.SetActive(true);
        }

        levelImage.SetActive(true);
        enabled = false;
    }
}
